﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository customeRepository;

        public CustomersController(ICustomerRepository customeRepository)
        {
            this.customeRepository = customeRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await customeRepository.GetAllAsync();
            var customersModelList = customers.Select(c =>
                new CustomerShortResponse()
                {
                    Id = c.Id,
                    LastName = c.LastName,
                    FirstName = c.FirstName,
                    Email = c.Email
                }).ToList();
            return customersModelList;
        }
        
        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customeRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                LastName = customer.LastName,
                FirstName = customer.FirstName,
                Email = customer.Email,
                Preferences = customer.CustomerPreferences?.Select(cp => 
                    new PreferenceResponse()
                    {
                        Id = cp.Preference.Id,
                        Name = cp.Preference.Name
                    })?.ToList(),
                PromoCodes = customer.PromoCodes?.Select(p =>
                    new PromoCodeShortResponse()
                    {
                        Id = p.Id,
                        BeginDate = p.BeginDate.ToShortDateString(),
                        Code = p.Code,
                        EndDate = p.EndDate.ToShortDateString(),
                        PartnerName = p.PartnerName,
                        ServiceInfo = p.ServiceInfo
                    })?.ToList()
            };
            return customerModel;
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                LastName = request.LastName,
                FirstName = request.FirstName,
                Email = request.Email
            };
            await customeRepository.AddAsync(customer);
            await customeRepository.UpdateCustomerPreferencesAsync(customer.Id, request.PreferenceIds);
            return Ok();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customeRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            (customer.LastName, customer.FirstName, customer.Email) = (request.LastName, request.FirstName, request.Email);
            await customeRepository.UpdateAsync(customer);
            await customeRepository.UpdateCustomerPreferencesAsync(customer.Id, request.PreferenceIds);
            return Ok();
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await customeRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            await customeRepository.DeleteAsync(id);
            return Ok();
        }
    }
}