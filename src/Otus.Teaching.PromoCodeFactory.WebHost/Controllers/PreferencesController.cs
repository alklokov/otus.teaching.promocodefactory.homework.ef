﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения пользователей
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            this.preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetAllAsync()
        {
            var prefs = await preferenceRepository.GetAllAsync();
            var prefResp = prefs?.Select(p =>
                new PreferenceResponse()
                {
                    Id = p.Id,
                    Name = p.Name
                }).ToList();
            return prefResp;
        }
    }
}
