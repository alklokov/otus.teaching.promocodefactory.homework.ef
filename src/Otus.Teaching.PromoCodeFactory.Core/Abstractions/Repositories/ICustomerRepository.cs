﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetAllAsync();
        
        Task<Customer> GetByIdAsync(Guid id);

        Task<Customer> AddAsync(Customer customer);

        Task UpdateAsync(Customer customer);

        Task DeleteAsync(Guid id);

        Task DeleteCustomerPreferencesAsync(Guid customerId);

        /// <summary>
        /// Сохраняем новые предпочтения пользователя (старые предварительно удаляем)
        /// </summary>
        /// <param name="customerId">Id пользователя</param>
        /// <param name="prefIds">Список Id новых предпочтений</param>
        /// <returns></returns>
        Task UpdateCustomerPreferencesAsync(Guid customerId, List<Guid> prefIds);

    }
}