﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ApplicationContext db;
        private readonly DbSet<Customer> customers;
        private readonly DbSet<CustomerPreference> customerPreferences;

        public CustomerRepository(ApplicationContext db)
        {
            this.db = db;
            customers = this.db.Set<Customer>();
            customerPreferences = this.db.Set<CustomerPreference>();
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await customers.AsNoTracking().ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            var customer = await customers
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                .ThenInclude(cp => cp.Preference)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();
            return customer;
        }

        public async Task<Customer> AddAsync(Customer customer)
        {
            if (customer.Id == null)
                customer.Id = Guid.NewGuid();
            await customers.AddAsync(customer);
            await db.SaveChangesAsync();
            return customer;
        }

        public async Task UpdateAsync(Customer customer)
        {
            db.Entry(customer).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            DeleteCustomerPromoCodes(id);
            DeleteCustomerPreferences(id);
            Customer c = await customers.FindAsync(id);
            if (c != null)
                customers.Remove(c);
            await db.SaveChangesAsync();
        }

        public async Task DeleteCustomerPreferencesAsync(Guid customerId)
        {
            DeleteCustomerPreferences(customerId);
            await db.SaveChangesAsync();
        }

        /// <summary>
        /// Сохраняем новые предпочтения пользователя (старые предварительно удаляем)
        /// </summary>
        /// <param name="customerId">Id пользователя</param>
        /// <param name="prefIds">Список Id новых предпочтений</param>
        /// <returns></returns>
        public async Task UpdateCustomerPreferencesAsync(Guid customerId, List<Guid> prefIds)
        {
            DeleteCustomerPreferences(customerId);
            if (prefIds != null && prefIds.Count > 0)
            {
                //Проверяем существование переданных предпочтений в БД
                var allPrefs = await db.Preferences.ToListAsync();
                var prefs = prefIds.Where(id => allPrefs.Any(p => p.Id == id))
                    .Select(id => new CustomerPreference() {CustomerId = customerId, PreferenceId = id})
                    .ToList();
                if (prefs != null && prefs.Count > 0)
                    await customerPreferences.AddRangeAsync(prefs);
            }
            await db.SaveChangesAsync();
        }

        //******************************************************************************************
        private void DeleteCustomerPreferences(Guid id)
            => customerPreferences.RemoveRange(customerPreferences.Where(cp => cp.Customer.Id == id));

        private void DeleteCustomerPromoCodes(Guid id)
            => db.PromoCodes.RemoveRange(db.PromoCodes.Where(pc => pc.Customer.Id == id));

    }
}
