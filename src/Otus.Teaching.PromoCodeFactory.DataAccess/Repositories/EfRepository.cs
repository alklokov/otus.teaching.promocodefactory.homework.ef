﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        private readonly ApplicationContext db;
        private readonly DbSet<T> dbSet;

        public EfRepository(ApplicationContext db)
        {
            db = db;
            dbSet = db.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
            => await dbSet.FindAsync(id);

        public async Task<T> AddAsync(T item)
        {
            if (item.Id == null)
                item.Id = Guid.NewGuid();
            await dbSet.AddAsync(item);
            await db.SaveChangesAsync();
            return item;
        }

        public async Task UpdateAsync(T item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            T item = await dbSet.FindAsync(id);
            if (item != null)
                dbSet.Remove(item);
            await db.SaveChangesAsync();
        }
    }
}
