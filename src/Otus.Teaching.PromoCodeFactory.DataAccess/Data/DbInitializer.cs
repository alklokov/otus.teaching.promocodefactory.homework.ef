﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationContext _context;

        public DbInitializer(ApplicationContext context)
            => _context = context;

        public void Initialize()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            
            _context.Employees.AddRange(FakeDataFactory.Employees);
            _context.Customers.AddRange(FakeDataFactory.Customers);
            //Roles.AddRange(FakeDataFactory.Roles);
            _context.Preferences.AddRange(FakeDataFactory.Preferences);
            _context.SaveChanges();

            AddPromoCodes();
            AddCustomerPreference();
        }

        public void AddPromoCodes()
        {
            var codes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef88c"),
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 01, 01),
                        Code = "Скидка 50%",
                        PartnerName = "Пироговый двор",
                        Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Семья")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef77c"),
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 01, 01),
                        Code = "Билет на премьеру",
                        PartnerName = "БДТ",
                        Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Театр"),
                        Customer = _context.Customers.FirstOrDefault(c => c.Email == "ivan_sergeev@mail.ru")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef66c"),
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 01, 01),
                        Code = "Пригласительный билет",
                        PartnerName = "БДТ",
                        Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Театр"),
                        PartnerManager = _context.Employees.FirstOrDefault(e => e.Email == "owner@somemail.ru")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef55c"),
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 01, 01),
                        Code = "Пригласительный билет",
                        PartnerName = "БДТ",
                        Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Театр"),
                        PartnerManager = _context.Employees.FirstOrDefault(e => e.Email == "owner@somemail.ru"),
                        Customer = _context.Customers.FirstOrDefault(c => c.Email == "ivan_sergeev@mail.ru")
                    }
                };
            _context.PromoCodes.AddRange(codes);
            _context.SaveChanges();
        }

        private void AddCustomerPreference()
        {
            _context.CustomerPreferences.AddRange(new List<CustomerPreference>()
            {
                new CustomerPreference()
                {
                    Id = Guid.Parse("ef7f199f-92d7-459f-896e-078ed53ef55c"),
                    Customer = _context.Customers.FirstOrDefault(c => c.Email == "ivan_sergeev@mail.ru"),
                    Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Театр")
                },
                new CustomerPreference()
                {
                    Id = Guid.Parse("ef7f199f-92d7-459f-896e-078ed53ef56c"),
                    Customer = _context.Customers.FirstOrDefault(c => c.Email == "ivan_sergeev@mail.ru"),
                    Preference = _context.Preferences.FirstOrDefault(p => p.Name == "Семья")
                }
            });
            _context.SaveChanges();
        }
    }
}
